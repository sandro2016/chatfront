import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { ICredentials } from './entity/credentials';
import { IUser } from './entity/user';
import { IToken } from './entity/token';
import { environment } from '../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private token: string;

  constructor(private http: HttpClient, private router: Router) { }

  baseUrl = environment.baseUrl;

  private saveToken(token: string): void {
    localStorage.setItem('token', token);
    this.token = token;
  }

  public getToken(): string {
    if (!this.token) {
      this.token = localStorage.getItem('token');
    }

    return this.token;
  }

  public getUser(): IUser {
    const token = this.getToken();
    let payload;
    if (token) {
      payload = token.split('.')[1];
      payload = window.atob(payload);
      return JSON.parse(payload);
    }

    return null;
  }

  public isLoggedIn(): boolean {
    const user = this.getUser();
    if (user) {
      return user.exp > Date.now() / 1000;
    }

    return false;
  }

  public isChatOwner(name: string): boolean {
    const user = this.getUser();
    if (!user) {
      return false;
    }

    if (user.name !== name) {
      return false;
    }

    return true;
  }

  public register(credentials: ICredentials): Observable<any> {
    return this.http.post(this.baseUrl + '/register', credentials).pipe(
      map((data: IToken) => {
        if (data.token) {
          this.saveToken(data.token);
        }
        return data;
      })
    );
  }

  public login(credentials: ICredentials): Observable<any> {
    return this.http.post(this.baseUrl + '/login', credentials).pipe(
      map((data: IToken) => {
        if (data.token) {
          this.saveToken(data.token);
        }
        return data;
      })
    );
  }

  public logout(): void {
    this.token = '';
    localStorage.removeItem('token');
    this.router.navigate(['/']);
  }
}
