import { Component, OnInit } from '@angular/core';
import { ICredentials } from '../entity/credentials';
import { UserService } from '../user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  credentials: ICredentials = {
    name: '',
    password: ''
  };

  constructor(private router: Router, private userService: UserService) { }

  ngOnInit() {
  }

  submit() {
    this.userService.login(this.credentials).subscribe(
      data => {
        // console.log(data);
        this.router.navigate(['/list']);
      },
      error => {
        console.log(error);
      }
    );
  }
}
