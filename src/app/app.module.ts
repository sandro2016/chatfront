import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { UserService } from './user.service';
import { ListComponent } from './list/list.component';
import { ChatService } from './chat.service';
import { NewComponent } from './new/new.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { ChatComponent } from './chat/chat.component';
import { AuthGuardService } from './auth-guard.service';

@NgModule({
   declarations: [
      AppComponent,
      HeaderComponent,
      HeaderComponent,
      HomeComponent,
      LoginComponent,
      RegisterComponent,
      ListComponent,
      NewComponent,
      NotFoundComponent,
      ChatComponent
   ],
   imports: [
      BrowserModule,
      AppRoutingModule,
      BrowserAnimationsModule,
      FormsModule,
      HttpClientModule
   ],
   providers: [
      UserService,
      ChatService,
      AuthGuardService
   ],
   bootstrap: [
      AppComponent
   ]
})
export class AppModule { }
