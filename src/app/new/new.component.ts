import { Component, OnInit } from '@angular/core';
import { ChatService } from '../chat.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-new',
  templateUrl: './new.component.html',
  styleUrls: ['./new.component.css']
})
export class NewComponent implements OnInit {

  private chat = {
    name: ''
  };

  constructor(private router: Router, private chatService: ChatService) { }

  ngOnInit() {
  }

  submit() {
    this.chatService.create(this.chat.name).subscribe(
      () => {
        this.router.navigate(['/list']);
      },
      error => {
        console.log(error);
      }
    );
  }

}
