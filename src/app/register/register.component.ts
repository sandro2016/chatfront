import { Component, OnInit } from '@angular/core';
import { ICredentials } from '../entity/credentials';
import { UserService } from '../user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  credentials: ICredentials = {
    name: '',
    password: ''
  };

  constructor(private router: Router, private userService: UserService) { }

  ngOnInit() {
  }

  submit() {
    this.userService.register(this.credentials).subscribe(
      data => {
        this.router.navigate(['/list']);
      },
      error => {
        console.log(error);
      }
    );
  }

}
