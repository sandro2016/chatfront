import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ChatService } from '../chat.service';
import { IChat } from '../entity/chat';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {

  private message: string;
  private messages: string[] = [];
  private id: number;
  private chat: IChat;

  constructor(private activatedRoute: ActivatedRoute, private chatService: ChatService) {
    this.id = this.activatedRoute.snapshot.params['id'];
  }

  ngOnInit() {
    this.chatService.get(this.id).subscribe(
      data => {
        this.chat = data;
      }
    );

    this.chatService.getMessages().subscribe(
      (message: string) => {
        this.messages.push(message);
      }
    );
  }

  sendMessage() {
    this.chatService.sendMessage(this.message);
    this.message = '';
  }
}
