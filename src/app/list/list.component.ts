import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ChatService } from '../chat.service';
import { UserService } from '../user.service';
import { IChat } from '../entity/chat';

class Chat implements IChat {
  id: number;
  name: string;
  owner: string;
}

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  dataSource: Chat[] = [];
  displayedColumns: string[] = ['name', 'owner', 'actions'];

  constructor(private router: Router, private userService: UserService, private chatService: ChatService) { }

  ngOnInit() {
    this.chatService.list().subscribe(data => {
      this.dataSource = data;
    });
  }

  delete(chat: Chat) {
    this.chatService.delete(chat.id).subscribe(() => {
      this.chatService.list().subscribe(data => {
        this.dataSource = data;
      });
    },
    error => {
      console.log(error);
    });
  }

  open(chat: Chat) {
    this.router.navigate(['/chat', chat.id]);
  }

}
