import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { environment } from '../environments/environment';
import { UserService } from './user.service';
import * as io from 'socket.io-client';

@Injectable({
  providedIn: 'root'
})
export class ChatService {

  private baseUrl = environment.baseUrl;
  private socket;

  constructor(private http: HttpClient, private userService: UserService) { }

  public list(): Observable<any> {
    return this.http.get<any>(this.baseUrl + '/chat/list', { headers: { Authorization: `Bearer ${this.userService.getToken()}` }}).pipe(
      map(
        (list) => list.map(
          chat => ({ id: chat.id, name: chat.name, owner: chat['user.name'] })
        )
      )
    );
  }

  public create(name: string): Observable<any> {
    const payload = {
      name,
      owner: this.userService.getUser().name
    };

    return this.http.post(this.baseUrl + '/chat/create', payload, { headers: { Authorization: `Bearer ${this.userService.getToken()}` }});
  }

  public delete(id: number): Observable<any> {
    return this.http.delete(
      this.baseUrl + '/chat/delete/' + id, { headers: { Authorization: `Bearer ${this.userService.getToken()}` }}
    );
  }

  public get(id: number): Observable<any> {
    this.socket = io(this.baseUrl);

    return this.http.get<any>(this.baseUrl + '/chat/' + id, { headers: { Authorization: `Bearer ${this.userService.getToken()}` }}).pipe(
      map(chat => {
        this.socket.emit('user joined', {room: chat.name, user: this.userService.getUser().name});

        return {id: chat.id, name: chat.name, owner: chat['user.name']};
      })
    );
  }

  public sendMessage(message: string) {
    this.socket.emit('new message', message);
  }

  public getMessages() {
    return new Observable((observer) => {
      this.socket.on('new message', (data) => {
        observer.next(data);
      });

      this.socket.on('user joined', (data) => {
        observer.next(data);
      });

      this.socket.on('user left', (data) => {
        observer.next(data);
      });
    });
  }

}
